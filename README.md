put\_date.vim
=============

This plugin provides a convenience wrapper around `strftime()` to insert the
current date and time in the buffer, defaulting to RFC2822 format, and
requiring neither percent signs nor quoting for any specified format.

    :PutDate
    Sun, 31 May 2020 01:04:41 +1200
    :PutDate d/m/Y
    31/05/2020

An attempt to use the UTC time zone rather than the local time zone is made if
a bang is added:

    :PutDate!
    Sat, 30 May 2020 13:04:14 +0000
    :PutDate!
    13

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
