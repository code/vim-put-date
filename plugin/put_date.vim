"
" put_date.vim: Convenience wrapper around `strftime()` to insert the current
" date and time in the buffer, with optional format and UTC switching.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_put_date') || &compatible || !exists('*strftime')
  finish
endif
let loaded_put_date = 1

" User command to put the date into the buffer at the current line
command! -bang -bar -nargs=* -range PutDate
      \ call put_date#(<q-line1>, <q-bang> ==# '!', <q-args>)
